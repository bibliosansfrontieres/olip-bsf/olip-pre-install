#!/bin/bash

ANSIBLECAP_PATH="/var/lib/ansible/local"
GIT_REPO_URL="https://gitlab.com/bibliosansfrontieres/olip/olip-pre-install.git"
ANSIBLE_BIN="/usr/bin/ansible-pull"
ANSIBLE_ETC="/etc/ansible/facts.d/"
BRANCH="master"

[ $EUID -eq 0 ] || {
    echo "Error: you have to be root to run this script." >&2
    exit 1
}

function internet_check()
{
    echo -n "[+] Check Internet connection... "
    if [[ ! `ping -q -c 2 gitlab.com` ]]
    then
        echo "ERROR: Repository is unreachable, check your Internet connection." >&2
        exit 1
    fi
    echo "Done."
}

function install_ansible()
{
    internet_check
    echo -e "[+] Install PPA... "
    apt update
    apt install --yes --upgrade software-properties-common ca-certificates
    apt-add-repository --yes --update ppa:ansible/ansible
    echo -e "[+] Update Ansible... "
    apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --quiet --quiet -y ansible=2.9* git
    echo 'Done.'
}

function clone_ansiblecube()
{
    echo -n "[+] Checking for internet connectivity... "
    internet_check
    echo 'Done.'

    echo -n "[+] Clone ansiblecap repo... "
    mkdir --mode 0755 -p ${ANSIBLECAP_PATH}
    cd ${ANSIBLECAP_PATH}/../
    git clone ${GIT_REPO_URL} local

    mkdir --mode 0755 -p ${ANSIBLE_ETC}
    cp ${ANSIBLECAP_PATH}/hosts /etc/ansible/hosts
    echo 'Done.'
}

[ -x /usr/bin/ansible ] || install_ansible
[ -d ${ANSIBLECAP_PATH} ] || clone_ansiblecube

echo "Checking file access" >> /var/log/ansible-pull.log
[ $? -ne 0 ] && echo "No space left to write logs or permission problem, exiting." && exit 1

while [[ $# -gt 0 ]]
do
    case $1 in
        -n|--name)

            if [ -z "$2" ]
            then
                echo -e "\n\t[+] ERROR\n\t--name : Missing device name\n"

                exit 0;
            fi
            PROJECT_NAME=$2

        shift
        ;;
        *)
            exit
        ;;
    esac
    shift
done

cd $ANSIBLECAP_PATH

echo "$ANSIBLE_BIN --purge -C $BRANCH -d $ANSIBLECAP_PATH -i hosts -U $GIT_REPO_URL main.yml --extra-vars \"project_name=$PROJECT_NAME\"" >> /var/lib/ansible/ansible-pull-cmd-line.sh
echo -e "\n[+] Start configuration...follow logs : tail -f /var/log/ansible-pull.log"

$ANSIBLE_BIN --purge -C $BRANCH -d $ANSIBLECAP_PATH -i hosts -U $GIT_REPO_URL main.yml --extra-vars "project_name=$PROJECT_NAME"
