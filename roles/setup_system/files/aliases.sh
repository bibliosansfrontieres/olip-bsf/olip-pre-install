#
# Ansible managed
#

# shellcheck disable=SC2148

# ls with bells'n'whistles
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -l'
alias ls='ls --color=auto --time-style=long-iso'

# grep with colors
alias bzegrep='bzegrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias bzfgrep='bzfgrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias bzgrep='bzgrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias egrep='egrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias fgrep='fgrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias zegrep='zegrep --color=auto'
alias zfgrep='zfgrep --color=auto'
alias zgrep='zgrep --color=auto'

# the sudo trick enables completion
alias sudo='sudo '

# sqlite for humans
alias sqlite3='sqlite3 -column -header'
